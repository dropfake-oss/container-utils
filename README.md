# container-utils

Container-Utils contains generic containers that have observable references to Uni.Rx.

## SingletonWithObservable

SingletonWithObservable properly implements the Microsoft recommended Disposal pattern - so that singletons can be torn down and recreated.
