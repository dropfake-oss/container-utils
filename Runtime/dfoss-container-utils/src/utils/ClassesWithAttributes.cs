using System;

namespace Container.Utils
{
	public abstract class ClassesWithAttributes<Klass, TAttribute>
		where Klass : class, new()
		where TAttribute : System.Attribute
	{
		private static Klass? _instance = null;

		public static Klass Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new Klass();
				}
				return _instance;
			}
		}

		private void FindClasses(System.Reflection.Assembly asm)
		{
			var attributeType = typeof(TAttribute);

			foreach (var type in asm.GetTypes())
			{
				if (type.GetCustomAttributes(attributeType, true) is TAttribute[] attributes)
				{
					if (attributes.Length > 0)
					{
						this.OnClassWithAttributes(type, attributes);
					}
				}
			}

		}

		private void Init()
		{
			foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
			{
				FindClasses(asm);
			}
		}

		protected ClassesWithAttributes()
		{
			Init();
		}

		protected abstract void OnClassWithAttributes(Type classWithAttributes, TAttribute[] attributes);
	}

}
