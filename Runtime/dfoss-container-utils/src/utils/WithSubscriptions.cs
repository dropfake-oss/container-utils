using System;

namespace Container.Utils
{
	public interface IWithSubscriptions
	{
		public void ClearSubscriptions();
	}

	public class WithSubscriptions : IDisposable, IWithSubscriptions
	{
		protected DisposableCollection<IDisposable> Subs = new DisposableCollection<IDisposable>();

		public virtual void Dispose()
		{
			this.ClearSubscriptions();
		}

		public void ClearSubscriptions()
		{
			this.Subs.Dispose();
		}
	}
}
