using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace Container.Utils
{
	public sealed class DisposableCollection : DisposableCollection<IDisposable>
	{

	}

	public class DisposableCollection<T> : Collection<T>, IDisposable
		where T : IDisposable
	{
		public DisposableCollection(IList<T> items)
			: base(items)
		{
		}

		public DisposableCollection()
		{
		}

		public void Dispose()
		{
			foreach (var item in this)
			{
				try
				{
					item.Dispose();
				}
				catch
				{
					// swallow
				}
			}
			this.Clear();
		}

		public void Reset()
		{
			this.Dispose();
		}

		public void AddDisposable(T disposable)
		{
			if (disposable == null)
			{
				return;
			}

			this.Add(disposable);
		}

		public void AddRange(IEnumerable<T>? disposables)
		{
			if (disposables == null)
			{
				return;
			}

			foreach (var disposable in disposables)
			{
				this.AddDisposable(disposable);
			}
		}
	}
}
