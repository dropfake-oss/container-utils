using System;
using UniRx;

namespace Container.Utils
{
	public class SingletonWithObservable<Klass> : IDisposable where Klass : class, new()
	{
		private static bool FirstNonNull = false;
		private bool Disposed = false;
		[Serializable]
		public class NotInitialized : Exception { }

		private static BehaviorSubject<Klass?> InstanceSubject = new BehaviorSubject<Klass?>(null);
		public static Klass Instance
		{
			get
			{
				var instance = InstanceSubject.Value;
				if (instance == null)
				{
					throw new NotInitialized();
				}
				return instance;
			}
		}
		public static IObservable<Klass?> AsObservable
		{
			get
			{
				return InstanceSubject.Where(instance => FirstNonNull);
			}
		}

		public static bool IsInitialized => InstanceSubject.Value != null;

		public static Klass Initialize()
		{
			var instance = new Klass();
			FirstNonNull = true;
			InstanceSubject.OnNext(instance);
			return instance;
		}

		protected SingletonWithObservable()
		{
		}

		protected virtual void Dispose(bool disposing)
		{
			if (this.Disposed)
			{
				return;
			}

			if (disposing)
			{
				// dispose managed state (managed objects).
				InstanceSubject.OnNext(null);
			}

			this.Disposed = true;
		}

		public void Dispose()
		{
			// Dispose of unmanaged resources.
			Dispose(true);
		}
	}

	public class SingletonWithObservable<Klass, TArg1> : IDisposable where Klass : class
	{
		private static bool FirstNonNull = false;
		private bool Disposed = false;
		[Serializable]
		public class NotInitialized : Exception { }

		private static BehaviorSubject<Klass?> InstanceSubject = new BehaviorSubject<Klass?>(null);
		public static Klass Instance
		{
			get
			{
				var instance = InstanceSubject.Value;
				if (instance == null)
				{
					throw new NotInitialized();
				}
				return instance;
			}
		}
		public static IObservable<Klass?> AsObservable
		{
			get
			{
				return InstanceSubject.Where(instance => FirstNonNull);
			}
		}

		public static bool IsInitialized => InstanceSubject.Value != null;

		protected SingletonWithObservable()
		{
		}

		public static Klass Initialize(TArg1 arg1, Func<TArg1, Klass> creator)
		{
			var instance = creator(arg1);
			FirstNonNull = true;
			InstanceSubject.OnNext(instance);
			return instance;
		}

		protected virtual void Dispose(bool disposing)
		{
			if (this.Disposed)
			{
				return;
			}

			if (disposing)
			{
				// dispose managed state (managed objects).
				InstanceSubject.OnNext(null);
			}

			this.Disposed = true;
		}

		public void Dispose()
		{
			// Dispose of unmanaged resources.
			Dispose(true);
		}
	}

	public class SingletonWithObservable<Klass, TArg1, TArg2> where Klass : class
	{
		private static bool FirstNonNull = false;
		private bool Disposed = false;
		[Serializable]
		public class NotInitialized : Exception { }

		private static BehaviorSubject<Klass?> InstanceSubject = new BehaviorSubject<Klass?>(null);
		public static Klass Instance
		{
			get
			{
				var instance = InstanceSubject.Value;
				if (instance == null)
				{
					throw new NotInitialized();
				}
				return instance;
			}
		}
		public static IObservable<Klass?> AsObservable
		{
			get
			{
				return InstanceSubject.Where(instance => FirstNonNull);
			}
		}

		public static bool IsInitialized => InstanceSubject.Value != null;

		protected SingletonWithObservable()
		{
		}

		public static Klass Initialize(TArg1 arg1, TArg2 arg2, Func<TArg1, TArg2, Klass> creator)
		{
			var instance = creator(arg1, arg2);
			FirstNonNull = true;
			InstanceSubject.OnNext(instance);
			return instance;
		}

		protected virtual void Dispose(bool disposing)
		{
			if (this.Disposed)
			{
				return;
			}

			if (disposing)
			{
				// dispose managed state (managed objects).
				InstanceSubject.OnNext(null);
			}

			this.Disposed = true;
		}

		public void Dispose()
		{
			// Dispose of unmanaged resources.
			Dispose(true);
		}
	}

	public class SingletonWithObservable<Klass, TArg1, TArg2, TArg3> : IDisposable where Klass : class
	{
		private static bool FirstNonNull = false;
		private bool Disposed = false;
		[Serializable]
		public class NotInitialized : Exception { }

		private static BehaviorSubject<Klass?> InstanceSubject = new BehaviorSubject<Klass?>(null);
		public static Klass Instance
		{
			get
			{
				var instance = InstanceSubject.Value;
				if (instance == null)
				{
					throw new NotInitialized();
				}
				return instance;
			}
		}
		public static IObservable<Klass?> AsObservable
		{
			get
			{
				return InstanceSubject.Where(instance => FirstNonNull);
			}
		}

		public static bool IsInitialized => InstanceSubject.Value != null;

		protected SingletonWithObservable()
		{
		}

		public static Klass Initialize(TArg1 arg1, TArg2 arg2, TArg3 arg3, Func<TArg1, TArg2, TArg3, Klass> creator)
		{
			var instance = creator(arg1, arg2, arg3);
			FirstNonNull = true;
			InstanceSubject.OnNext(instance);
			return instance;
		}

		protected virtual void Dispose(bool disposing)
		{
			if (this.Disposed)
			{
				return;
			}

			if (disposing)
			{
				// dispose managed state (managed objects).
				InstanceSubject.OnNext(null);
			}

			this.Disposed = true;
		}

		public void Dispose()
		{
			// Dispose of unmanaged resources.
			Dispose(true);
		}
	}

	public class SingletonWithObservable<Klass, TArg1, TArg2, TArg3, TArg4> : IDisposable where Klass : class
	{
		private static bool FirstNonNull = false;
		private bool Disposed = false;
		[Serializable]
		public class NotInitialized : Exception { }

		private static BehaviorSubject<Klass?> InstanceSubject = new BehaviorSubject<Klass?>(null);
		public static Klass Instance
		{
			get
			{
				var instance = InstanceSubject.Value;
				if (instance == null)
				{
					throw new NotInitialized();
				}
				return instance;
			}
		}
		public static IObservable<Klass?> AsObservable
		{
			get
			{
				return InstanceSubject.Where(instance => FirstNonNull);
			}
		}

		public static bool IsInitialized => InstanceSubject.Value != null;

		protected SingletonWithObservable()
		{
		}

		public static Klass Initialize(TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, Func<TArg1, TArg2, TArg3, TArg4, Klass> creator)
		{
			var instance = creator(arg1, arg2, arg3, arg4);
			FirstNonNull = true;
			InstanceSubject.OnNext(instance);
			return instance;
		}

		protected virtual void Dispose(bool disposing)
		{
			if (this.Disposed)
			{
				return;
			}

			if (disposing)
			{
				// dispose managed state (managed objects).
				InstanceSubject.OnNext(null);
			}

			this.Disposed = true;
		}

		public void Dispose()
		{
			// Dispose of unmanaged resources.
			Dispose(true);
		}
	}
}
