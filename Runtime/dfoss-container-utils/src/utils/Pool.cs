using System;
using System.Collections.Generic;

namespace Container.Utils
{
	public class Pool<T> where T : class
	{
		private readonly Stack<T> _pool;

		private readonly Func<T> _onCreate;
		private readonly Action<T>? _onReset;
		private readonly Action<T> _onRelease;

		public Pool(int capacity, Func<T> onCreate, Action<T>? onReset = null)
		{
			_pool = new Stack<T>(capacity);
			this._onCreate = onCreate;
			this._onReset = onReset;
			this._onRelease = Release;

			for (int i = 0; i < capacity; ++i)
			{
				_pool.Push(_onCreate());
			}
		}

		public T Get()
		{
			return _pool.Count > 0 ? _pool.Pop() : _onCreate();
		}

		public void Release(T value)
		{
			if (_onReset != null)
			{
				_onReset(value);
			}

			_pool.Push(value);
		}

		// Returns a DisposablePooledObject that will automatically return the instance to the pool when it is disposed.
		// ex: using (var pooledObject = pool.Get(out PooledClass automaticallyReleasedPooledList))
		public DisposablePooledObject<T> Get(out T value)
		{
			value = Get();
			return DisposablePooledObject<T>.Get(value, _onRelease);
		}
	}

	public class DisposablePooledObject<T> : IDisposable where T : class
	{
		private Action<T> _onDispose;
		public T? Value { get; private set; }

		private static readonly Pool<DisposablePooledObject<T>> _pool = new Pool<DisposablePooledObject<T>>(2,
			 () => new DisposablePooledObject<T>(),
			 wrapper =>
			 {
				 if( wrapper.Value != null )
				 {
					 wrapper._onDispose(wrapper.Value);
				 }
				 wrapper._onDispose = t => { };
				 wrapper.Value = default(T);
			 }
		 );

		public static DisposablePooledObject<T> Get(T value, Action<T> dispose)
		{
			var wrapper = _pool.Get();
			wrapper.Value = value;
			wrapper._onDispose = dispose;
			return wrapper;
		}

		private DisposablePooledObject()
		{
			this._onDispose = t => { };
		}

		public void Dispose()
		{
			_pool.Release(this);
		}
	}
}
