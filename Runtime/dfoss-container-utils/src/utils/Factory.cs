using System;
using System.Collections.Generic;
using System.Linq;
namespace Container.Utils
{
	public class OrderedAttribute : System.Attribute
	{
		public int Order = 0;
	}

	public class Factory<Klass, Fn> : BaseFactory<Fn>
		where Klass : class, new()
		where Fn : System.Delegate
	{
		private static Klass? _instance = null;

		public static Klass Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new Klass();
				}
				return _instance;
			}
		}

		protected Factory(string fnName, System.Type attributeType)
		{
			Init(fnName, attributeType);
		}
	}

	public class BaseFactory<Fn> where Fn : System.Delegate
	{
		private struct OrderedFn
		{
			public Fn fn;
			public int order;
			public string name;
		}

		private List<Fn>? _fns;

		private int GetOrder(object[] attributes)
		{
			foreach (var attr in attributes)
			{
				var ordered = attr as OrderedAttribute;
				if (ordered != null)
				{
					return ordered.Order;
				}
			}

			return 0;
		}

		private void FindClasses(System.Reflection.Assembly asm, string fnName, System.Type attributeType, List<OrderedFn> fns)
		{
			foreach (var type in asm.GetTypes())
			{
				var attributes = type.GetCustomAttributes(attributeType, true);
				if (attributes.Length > 0)
				{
					var method = type.GetMethod(fnName, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
					if (method != null)
					{
						try
						{
							var fn = (Fn)method.CreateDelegate(typeof(Fn));
							fns.Add(new OrderedFn { fn = fn, order = GetOrder(attributes), name = type.Name });
						}
						catch
						{
							throw new System.Exception($"Unabled to bind function {type.Name}::{fnName}, the parameters don't match!");
						}
					}
				}
			}

		}

		protected void Init(string fnName, System.Type attributeType)
		{
			var fns = new List<OrderedFn>();
			foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
			{
				FindClasses(asm, fnName, attributeType, fns);
			}
			fns.Sort((a, b) =>
			{
				if (a.order == b.order)
				{
					return a.name.CompareTo(b.name);
				}
				return a.order.CompareTo(b.order);
			});
			_fns = fns.Select(fn => fn.fn).ToList();
		}

		public IReadOnlyList<Fn>? Registered { get { return _fns; } }
	}

}
