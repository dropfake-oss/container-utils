using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Container.Utils
{
	public class Multimap<T, V> : IReadOnlyDictionary<T, List<V>> where T : notnull
	{
		private Dictionary<T, List<V>> _dictionary;
		private int _listCapacity;

		public Multimap()
		{
			_dictionary = new Dictionary<T, List<V>>();
		}

		public Multimap(int keyCapacity, int listCapacity)
		{
			_dictionary = new Dictionary<T, List<V>>(keyCapacity);
			_listCapacity = listCapacity;
		}

		public void Add(T key, V value)
		{
			List<V>? list;
			if (this._dictionary.TryGetValue(key, out list))
			{
				list.Add(value);
			}
			else
			{
				list = new List<V>(_listCapacity); // TODO alloc
				list.Add(value);
				this._dictionary[key] = list;
			}
		}

		bool IReadOnlyDictionary<T, List<V>>.ContainsKey(T key)
		{
			return _dictionary.ContainsKey(key);
		}

		bool IReadOnlyDictionary<T, List<V>>.TryGetValue(T key, [MaybeNullWhen(false)] out List<V> value)
		{
			return _dictionary.TryGetValue(key, out value);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _dictionary.GetEnumerator();
		}

		IEnumerator<KeyValuePair<T, List<V>>> IEnumerable<KeyValuePair<T, List<V>>>.GetEnumerator()
		{
			return _dictionary.GetEnumerator();
		}

		public IEnumerable<T> Keys
		{
			get
			{
				return this._dictionary.Keys;
			}
		}

		IEnumerable<List<V>> IReadOnlyDictionary<T, List<V>>.Values => _dictionary.Values;

		int IReadOnlyCollection<KeyValuePair<T, List<V>>>.Count => _dictionary.Count;

		public List<V> this[T key]
		{
			get
			{
				List<V>? list;
				if (this._dictionary.TryGetValue(key, out list))
				{
					return list;
				}

#pragma warning disable CS8603
				return null;
#pragma warning restore CS8603
			}
		}
	}
}
