using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;

namespace Container.Utils
{
	public interface IReactiveItem<T>
	{
		IObservable<T> OnItemChanged { get; }
	}

	// TPC: in order for this class to derive AddRange from its base class - it would need to extend List<T>
	public class ReactiveList<T> : IList<T>, IDisposable where T : notnull
	{
		private List<T> _list = new List<T>();
		private ReplaySubject<IList<T>> _subject = new ReplaySubject<IList<T>>(1);
		private Dictionary<T, IDisposable> _subs = new Dictionary<T, IDisposable>();

		public IObservable<IList<T>> OnChanged => _subject;

		public ReactiveList()
		{
			Notify();
		}

		~ReactiveList()
		{
			Dispose();
		}

		#region IList Implementation
		public T this[int index]
		{
			get
			{
				return _list[index];
			}
			set
			{
				_list[index] = value;
				Notify();
			}
		}

		public int Count
		{
			get { return _list.Count; }
		}

		public void Add(T item)
		{
			_list.Add(item);
			Subscribe(item);
			Notify();
		}

		public void Clear()
		{
			if (Count == 0)
			{
				return;
			}
			ClearSubs();
			_list.Clear();
			Notify();
		}

		public bool Contains(T item)
		{
			return _list.Contains(item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			_list.CopyTo(array, arrayIndex);
		}

		public IEnumerator<T> GetEnumerator()
		{
			return _list.GetEnumerator();
		}

		public int IndexOf(T item)
		{
			return _list.IndexOf(item);
		}

		public void Insert(int index, T item)
		{
			_list.Insert(index, item);
			Subscribe(item);
			Notify();
		}

		public bool Remove(T item)
		{
			bool removed = _list.Remove(item);
			if (removed)
			{
				Unsubscribe(item);
				Notify();
			}
			return removed;
		}

		public void RemoveAt(int index)
		{
			var value = _list[index];
			_list.RemoveAt(index);
			Unsubscribe(value);
			Notify();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _list.GetEnumerator();
		}
		#endregion

		#region List Implementation
		public void AddRange(IEnumerable<T> collection)
		{
			foreach (var item in collection)
			{
				_list.Add(item);
				Subscribe(item);
			}
			Notify();
		}
		#endregion

		public void ClearAndAddRange(IEnumerable<T> collection)
		{
			_list.Clear();
			ClearSubs();
			AddRange(collection);
		}

		public void ClearAndAdd(T item)
		{
			if (Count > 0)
			{
				ClearSubs();
				_list.Clear();
			}

			_list.Add(item);
			Subscribe(item);

			Notify();
		}

		public void Union(IEnumerable<T> collection)
		{
			foreach (var item in collection)
			{
				if (!Contains(item))
				{
					_list.Add(item);
					Subscribe(item);
				}
			}
			Notify();
		}

		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		private void Subscribe(T item)
		{
			var reactive = item as IReactiveItem<T>;
			if (reactive != null)
			{
				_subs.TryGetValue(item, out IDisposable? sub);
				sub = sub ?? reactive.OnItemChanged.Subscribe(this.OnItemChanged);
				_subs[item] = sub;
			}
		}

		private void Unsubscribe(T item)
		{
			if (_subs.TryGetValue(item, out IDisposable? sub))
			{
				sub.Dispose();
				_subs.Remove(item);
			}
		}

		private void ClearSubs()
		{
			foreach (var sub in _subs.Values)
			{
				sub.Dispose();
			}
			_subs.Clear();
		}

		void OnItemChanged(T item)
		{
			Notify();
		}

		void Notify()
		{
			_subject?.OnNext(this);
		}

		public void Dispose()
		{
			ClearSubs();
			_list.Clear();
			_subject?.Dispose();

#pragma warning disable CS8625
			_subject = null;
#pragma warning restore CS8625
		}
	}
}
