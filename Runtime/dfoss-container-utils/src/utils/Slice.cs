﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Container.Utils
{
	public static class Slice
	{
		[return: NotNullIfNotNull("slice")]
		public static List<T>? Append<T>(List<T>? slice, T entry)
		{
			if (entry == null)
			{
				return slice;
			}

			if (slice == null)
			{
				slice = new List<T>();
			}

			slice.Add(entry);
			return slice;
		}

		[return: NotNullIfNotNull("slice")]
		public static List<T>? Append<T>(List<T>? slice, IEnumerable<T>? entries)
		{
			if (entries == null)
			{
				return slice;
			}

			if (slice == null)
			{
				slice = new List<T>();
			}

			slice.AddRange(entries);
			return slice;
		}

		[return: NotNullIfNotNull("slice")]
		public static Stack<T>? Append<T>(Stack<T>? slice, T entry)
		{
			if (entry == null)
			{
				return slice;
			}

			if (slice == null)
			{
				slice = new Stack<T>();
			}

			slice.Push(entry);
			return slice;
		}

		[return: NotNullIfNotNull("slice")]
		public static Stack<T>? Append<T>(Stack<T>? slice, IEnumerable<T>? entries)
		{
			if (entries == null)
			{
				return slice;
			}

			if (slice == null)
			{
				slice = new Stack<T>();
			}

			foreach (var entry in entries)
			{
				slice.Push(entry);
			}
			return slice;
		}

		[return: NotNullIfNotNull("slice")]
		public static Queue<T>? Append<T>(Queue<T>? slice, T entry)
		{
			if (entry == null)
			{
				return slice;
			}

			if (slice == null)
			{
				slice = new Queue<T>();
			}

			slice.Enqueue(entry);
			return slice;
		}

		[return: NotNullIfNotNull("slice")]
		public static Queue<T>? Append<T>(Queue<T>? slice, IEnumerable<T>? entries)
		{
			if (entries == null)
			{
				return slice;
			}

			if (slice == null)
			{
				slice = new Queue<T>();
			}

			foreach (var entry in entries)
			{
				slice.Enqueue(entry);
			}
			return slice;
		}

		public static int Len<T>(T[]? slice)
		{
			if (slice == null)
			{
				return 0;
			}

			return slice.Length;
		}

		public static int Len<T>(List<T>? slice)
		{
			if (slice == null)
			{
				return 0;
			}

			return slice.Count;
		}

		public static int Len<T>(IReadOnlyCollection<T>? slice)
		{
			if (slice == null)
			{
				return 0;
			}

			return slice.Count;
		}

		public static int Len<T>(ICollection<T>? slice)
		{
			if (slice == null)
			{
				return 0;
			}

			return slice.Count;
		}
	}
}
