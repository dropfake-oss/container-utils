﻿using System.Collections.Generic;

namespace Container.Utils.Extensions
{
	public static class ListExtensions
	{
		// Erase back, removes from the end of the array so as not to copy items when removing in the middle
		public static bool EraseBack<T>(this List<T> p, T item)
		{
			var index = p.IndexOf(item);
			if (index >= 0)
			{
				var last = p.Count - 1;
				if (last != index)
				{
					p[index] = p[last];
				}
				p.RemoveAt(last);
				return true;
			}
			return false;
		}

		public static int FindIndex<T>(this IList<T> source, System.Predicate<T> match)
		{
			for (int i = 0; i < source.Count; ++i)
			{
				if (match(source[i]))
				{
					return i;
				}
			}
			return -1;
		}
	}


}
